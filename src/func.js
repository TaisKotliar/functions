const getSum = (str1, str2) => {
  if (typeof str1 != 'string' || typeof str2 != 'string')
    return false;
  const exp = /^\d*$/;
  if (!exp.test(str1) || !exp.test(str2)) return false; 
  return (BigInt(str1)+BigInt(str2)).toString();
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const posts = listOfPosts.filter(n => n.author === authorName);
  let comments = 0;
  const filt = Array.from(listOfPosts.filter(n=>n.comments !== undefined), n=>n.comments);
  for (const ar of filt)
    comments += ar.filter(n=>n.author === authorName).length;
  return `Post:${posts.length},comments:${comments}`;
}

const tickets=(people)=> {
  let cash = 0;
  for (const bill of people)
  {
    if (bill === 25) cash+= bill;
    else{
      cash-= bill-25;
      if (cash < 0) return 'NO';
      cash+= bill;
    } 
  }
  return 'YES';
}


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
